import english from './en';
import deutch from './de';

export default
{
  en: english,
  de: deutch,
};
